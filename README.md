Calypso is the FASER offline software system, based on the ATLAS Athena and LHCb GAUDI frameworks

Start by creating a personal fork of https://gitlab.cern.ch/faser/calypso.git (using the gitlab web interface)

Then the following sequence will allow you to compile and run Calypso on any machine with cvmfs access.
```
#clone the (forked) project to your local machine
git clone --recursive https://:@gitlab.cern.ch:8443/$USERNAME/calypso.git 

#The next three lines are used to setup the ATLAS release environment
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase 
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup --input=calypso/asetup.faser Athena,22.0.31

#create build directory
mkdir build
cd build

#build calypso
cmake -DCMAKE_INSTALL_PREFIX=../run ../calypso ; make ; make install
```

Before running anything, it is MANDATORY to do:

```
cd ../run
source ./setup.sh
```

Don't omit the dot in the `source ./setup.sh` command! 

It can be convenient to alias the "asetup --input=calypso/asetup.faser" to something like "fsetup"

## Known issues:

* It is now essential to use the tags `ConfigFlags.GeoModel.FaserVersion` and `ConfigFlags.IOVDb.GlobalTag` in a consistent way.  If nothing is specified the first option (baseline) should be chosen by default.

** `ConfigFlags.GeoModel.FaserVersion = "FASER-01"` and `ConfigFlags.IOVDb.GlobalTag = OFLCOND-FASER-01` enables the baseline TI-12 detector

** `ConfigFlags.GeoModel.FaserVersion = "FASER-02"` and `ConfigFlags.IOVDb.GlobalTag = OFLCOND-FASER-02` enables the interface tracker and repositioned Veto

** `ConfigFlags.GeoModel.FaserVersion = "FASERNU-02"` and `ConfigFlags.IOVDb.GlobalTag = OFLCOND-FASER-02` enables the full FaserNu (IFT + emulsion) setup

* The "FaserActsKalmanFilter" package is temporarily disabled.

* The command `lsetup "lcgenv -p LCG_98python3_ATLAS_8 x86_64-centos7-gcc8-opt sqlite"` may be necessary to avoid errors when generating a database
