/*
  Copyright (C) 2021 CERN for the benefit of the FASER collaboration
*/

#ifndef SPACEPOINTCNV_P0_H
#define SPACEPOINTCNV_P0_H

#include "TrackerSpacePoint/FaserSCT_SpacePoint.h"
#include "TrackerEventTPCnv/SpacePoint_p0.h"

#include "AthenaPoolCnvSvc/T_AthenaPoolTPConverter.h"

class SpacePointCnv_p0 : public T_AthenaPoolTPCnvBase<Tracker::FaserSCT_SpacePoint, SpacePoint_p0> {
 public:
  SpacePointCnv_p0() {};

  virtual void persToTrans(const SpacePoint_p0* persObj,
                           Tracker::FaserSCT_SpacePoint* transObj,
                           MsgStream& log);

  virtual void transToPers(const Tracker::FaserSCT_SpacePoint* transObj,
                           SpacePoint_p0* persObj,
                           MsgStream& log);
};

#endif  // SPACEPOINTCNV_P0_H
