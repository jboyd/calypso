/*
  Copyright (C) 2002-2017 CERN for the benefit of the ATLAS collaboration
*/

#ifndef FASERSIHITCNV_P1_H
#define FASERSIHITCNV_P1_H

/*
Transient/Persistent converter for FaserSiHit class
*/

#include "TrackerSimEvent/FaserSiHit.h"
#include "FaserSiHit_p1.h"

#include "AthenaPoolCnvSvc/T_AthenaPoolTPConverter.h"

class MsgStream;


class FaserSiHitCnv_p1  : public T_AthenaPoolTPCnvBase<FaserSiHit, FaserSiHit_p1>
{
public:

  FaserSiHitCnv_p1() {}

  virtual void          persToTrans(const FaserSiHit_p1* persObj, FaserSiHit*
transObj, MsgStream &log);
  virtual void          transToPers(const FaserSiHit* transObj, FaserSiHit_p1*
persObj, MsgStream &log);
};


#endif
