#ifndef XAODFASERTRACKINGATHENAPOOL_XAODSTRIPCLUSTERAUXCONTAINERCNV_H
#define XAODFASERTRACKINGATHENAPOOL_XAODSTRIPCLUSTERAUXCONTAINERCNV_H

// Gaudi/Athena include(s):
#include "AthenaPoolCnvSvc/T_AthenaPoolCustomCnv.h"

// EDM include(s):
#include "xAODFaserTracking/StripClusterAuxContainer.h"

/// Base class for the converter
typedef T_AthenaPoolCustomCnvWithKey< xAOD::StripClusterAuxContainer,
                               xAOD::StripClusterAuxContainer >
   xAODStripClusterAuxContainerCnvBase;

/**
 *  @short POOL converter for the xAOD::StripClusterAuxContainer class
 *
 */
class xAODStripClusterAuxContainerCnv :
   public xAODStripClusterAuxContainerCnvBase {

public:
   /// Converter constructor
   xAODStripClusterAuxContainerCnv( ISvcLocator* svcLoc, const char* name = nullptr );

protected:
   /// Function preparing the container to be written out
   virtual xAOD::StripClusterAuxContainer*
   createPersistentWithKey( xAOD::StripClusterAuxContainer* trans, const std::string& key );
   /// Function reading in the object from the input file
   virtual xAOD::StripClusterAuxContainer* createTransientWithKey( const std::string& key );

}; // class xAODStripClusterAuxContainerCnv

#endif 

