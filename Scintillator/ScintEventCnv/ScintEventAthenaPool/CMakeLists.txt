# $Id: CMakeLists.txt 749562 2016-05-25 04:45:43Z krasznaa $
################################################################################
# Package: ScintEventAthenaPool
################################################################################

# Declare the package name:
atlas_subdir( ScintEventAthenaPool )

# Component(s) in the package:
atlas_add_poolcnv_library( ScintEventAthenaPoolPoolCnv
   ScintEventAthenaPool/*.h src/*.h src/*.cxx
   FILES ScintRawEvent/ScintWaveform.h
   ScintRawEvent/ScintWaveformContainer.h
   LINK_LIBRARIES Identifier GeneratorObjectsTPCnv AthAllocators AthContainers
   AthenaBaseComps AthenaKernel SGTools StoreGateLib AthenaPoolCnvSvcLib
   AthenaPoolUtilities AtlasSealCLHEP GaudiKernel ScintRawEvent
   )

atlas_add_dictionary( ScintEventAthenaPoolCnvDict
   ScintEventAthenaPool/ScintEventAthenaPoolCnvDict.h
   ScintEventAthenaPool/selection.xml
   LINK_LIBRARIES Identifier GeneratorObjectsTPCnv )

# Install files from the package:
atlas_install_headers( ScintEventAthenaPool )



