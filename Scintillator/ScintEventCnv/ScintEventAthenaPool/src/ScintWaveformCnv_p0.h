/*
  Copyright (C) 2020 CERN for the benefit of the FASER collaboration
*/

#ifndef SCINTWAVEFORMCNV_P0_H
#define SCINTWAVEFORMCNV_P0_H

#include "ScintRawEvent/ScintWaveform.h"
#include "ScintEventAthenaPool/ScintWaveform_p0.h"

#include "AthenaPoolCnvSvc/T_AthenaPoolTPConverter.h"

class ScintWaveformCnv_p0 : public T_AthenaPoolTPCnvBase<ScintWaveform, ScintWaveform_p0> {
 public:
  ScintWaveformCnv_p0() {};

  virtual void persToTrans(const ScintWaveform_p0* persObj,
			   ScintWaveform* transObj,
			   MsgStream& log);

  virtual void transToPers(const ScintWaveform* transObj,
			   ScintWaveform_p0* persObj,
			   MsgStream& log);
 private:
};

#endif
