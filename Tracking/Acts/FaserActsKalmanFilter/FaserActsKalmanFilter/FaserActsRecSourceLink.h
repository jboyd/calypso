# pragma once

// ACTS
#include "Acts/EventData/Measurement.hpp"
#include "TrackParameterization.hpp"

// ATHENA
#include "TrkSpacePoint/SpacePoint.h"


class RecSourceLink {
public:
    // TODO convert Trk::LocalParameters to acts::BoundVector and Amg::MatrixX& to ACTS::BoundMatrix
    RecSourceLink(const Acts::Surface& surface, const Trk::SpacePoint& spacePoint)
        : m_values(spacePoint.localParameters()),
          m_cov(spacePoint.localCovariance()),
          m_surface(&surface),
          m_spacePoint(&spacePoint) {}

    RecSourceLink() = default;

    constexpr const Acts::Surface& referenceSurface() const { return *m_surface; }
    constexpr const Trk::SpacePoint& spacePoint() const { return *m_spacePoint; }

    Acts::FittableMeasurement<RecSourceLink> operator*() const {
        return Acts::Measurement<RecSourceLink, Acts::BoundIndices, Acts::eBoundLoc0, Acts::eBoundLoc1> {
            m_surface->getSharedPtr(),
            *this,
            m_cov.topLeftCorner<2, 2>(),
            m_values[0],
            m_values[1]
        };
    }

    Acts::GeometryIdentifier geometryId() const
    {
        return m_surface->geometryId();
    }

private:
    Acts::BoundVector m_values;
    Acts::BoundMatrix m_cov;
    const Acts::Surface* m_surface;
    const Trk::SpacePoint* m_spacePoint;

    friend constexpr bool operator==(const RecSourceLink& lhs, const RecSourceLink& rhs) {
        return lhs.m_spacePoint == rhs.m_spacePoint;
    }
    friend constexpr bool operator!=(const RecSourceLink& lhs, const RecSourceLink& rhs) {
        return !(lhs == rhs);
    }


};
